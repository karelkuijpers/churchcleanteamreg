<?php
namespace Parousia\Churchcleanteamreg\Domain\Model;

/***
 *
 * This file is part of the "Cleanteamreg" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * 
 */
class Cleanteam extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * weekgeenschoonmaak
     *
     * @var string
     */
    protected $weekgeenschoonmaak = '';

   /**
     * Returns weekgeenschoonmaak
     *
     * @return string $weekgeenschoonmaak
     */
    public function getWeekgeenschoonmaak()
    {
        return $this->weekgeenschoonmaak;
    }

    /**
     * Sets the tstamp
     *
     * @param string $weekgeenschoonmaak
     * @return void
     */
    public function setWeekgeenschoonmaak( $weekgeenschoonmaak)
    {
        $this->weekgeenschoonmaak = $weekgeenschoonmaak;
    }
}
