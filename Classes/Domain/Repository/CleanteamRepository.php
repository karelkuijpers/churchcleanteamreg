<?php

namespace Parousia\Churchcleanteamreg\Domain\Repository;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;
use TYPO3\CMS\Extbase\Persistence\Repository;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;


/**
 * Class CleanteamRepository
 *
 * @package Parousia\Churchcleanteamreg\Domain\Repository
 *
 * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
 */
class CleanteamRepository extends Repository
{
	var $ErrMsg=""; //error message
	var $personReg=false;
	var $cleanteamReg=false;
	var $userid="";
	var $query;
	protected $aExport;

	
	public function findBediening(string $omschrijving = null, int $id_parent = null, $order='desc')
	{
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(TRUE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);
		$statement = "SELECT `omschrijving`, `uid` FROM `bediening` WHERE `omschrijving` LIKE '".$omschrijving."%' ";
		if ($id_parent) {
			$statement .= " AND `id_parent` = $id_parent ";
        } 
        $statement .= "ORDER BY `omschrijving` ".$order." LIMIT 1";

        $query->statement($statement);
        $result= $query->execute(true);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findBediening statement: '.$statement.'; result:'.urldecode(http_build_query($result[0],NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
        return $result[0];

        }

	public function findCoordinators(\DateTime $startnewseason, int $case,int $bedieningid)
	{
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(TRUE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);
		$cond="like 'cleanteam%'";
		if ($case==3 or $case==4)$cond=">= '".$startnewseason->format('Y-').str_pad($startnewseason->format('W'), 2, '0', STR_PAD_LEFT)." & '";
		$statement = "select group_concat(distinct(id_bedieningsleider) separator ',' ) as coords from bediening where id_parent=".$bedieningid." and omschrijving ".$cond." and id_bedieningsleider>'0'";
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findCoordinators statement: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');

        $query->statement($statement);
        $result= $query->execute(true);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findCoordinators result:'.urldecode(http_build_query($result[0],NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
        return $result[0]['coords'];

        }

	public function findAdresses(\DateTime $now)
	{
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(TRUE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);
	
		$jaaroud=($now->format('Y')-79).'-09-01';
		$jaarjong=($now->format('Y')-7).'-09-01';

		$select_fields="a.uid,a.postcode,a.huisnummer , group_concat(cast(p.uid as char) separator ',') as persids";
		$from_table="adres a,persoon p";
		$where_clause="a.deleted=0 and (a.land='' or a.land='Nederland') and AES_DECRYPT(p.id_adres,@password)=a.uid and p.deleted=0 and ".
		"bezoeker='is bezoeker' and cleanteam='kan meedoen'".
		" and (p.geboortedatum>'".$jaaroud."' and p.geboortedatum<='".$jaarjong."' or p.geboortedatum='0000-00-00')";
		$groupBy ="a.uid";
		$orderBy = 'postcode, huisnummer';
		$adressesldr= array();
		// Selecteer alle adressen met personen:
		$statement="select ".$select_fields." from ".$from_table." where ".$where_clause." group by ".$groupBy." order by ".$orderBy;
        $query->statement($statement);
        $result= $query->execute(true);
        return $result;
	}
	/*
	8 insertBedieningTaak
	* return array(teambediening_id,taak_id)
	*/
	public function insertBedieningTaak(&$db , string $omschrijving,$updatebasic_bediening,$updatebasic__taak)
	{
		$ids=array();
		if (empty($db))churchpersreg_div::connectdb($db);

		$statement="insert into bediening set omschrijving='".$omschrijving."',".$updatebasic_bediening;
		$db->query($statement);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'insertBediening statement:'.$statement.'; error: '.$db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
		$ids['teambediening_id']=$db->insert_id;
		$statement="insert into taak set omschrijving='".$omschrijving."',id_parent=".$ids['teambediening_id'].",".$updatebasic__taak;
		$db->query($statement);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'insertTaak statement:'.$statement.'; error: '.$db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
		$ids['taak_id']=$db->insert_id;
		return $ids;					
	}
	
	/*
	8 insertTaakbekleding
	* return result
	*/
	public function insertTaakbekleding(&$db, int $taak_id, int $id_persoon,string $datum_start)
	{
		if (empty($db))churchpersreg_div::connectdb($db);

		$statement="insert into taakbekleding set id_parent=".$taak_id.",id_persoon=".$id_persoon.",datum_start='".$datum_start."'";
		$result=$db->query($statement);
		return $result;					
		
	}
	/*
	8 updateBedieningsleider
	* return result
	*/
	public function updateBedieningsleider(&$db, int $teambediening_id, int $pers)
	{
		if (empty($db))churchpersreg_div::connectdb($db);
		$id_bedieningsleider="if(`id_bedieningsleider`>'0',concat(`id_bedieningsleider`,',',".$pers."),".$pers.")";
		$statement="update bediening set id_bedieningsleider=".$id_bedieningsleider." where uid=".$teambediening_id;
		$result=$db->query($statement);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'updateBedieningsleider statement:'.$statement.'; error: '.$db->error."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
		return $result;					
		
	}

	/*
	8 updateBedieningsleider
	* return result
	*/
	public function removeCleanteam(&$db, int $bedieningid, string $cond)
	{
		if (empty($db))churchpersreg_div::connectdb($db);
		// verwijder alle cleanteams medewerkers van lopend seizoen om opnieuw samen te stellen of oud seizoen::
		$statement="delete from taakbekleding where id_parent in (select uid from taak where ".$cond." and id_parent in (select uid from bediening where id_parent = ".$bedieningid."))";
		$result=$db->query($statement);
		if ($result) {
			// verwijder alle cleamteam taken
			$statement="delete from taak where ".$cond." and id_parent in (select uid from bediening where id_parent = ".$bedieningid.")";	
			$result=$db->query($statement);
		}
		if ($result) {
			// verwijder alle cleamteam bedieningen
			$statement="delete from bediening where ".$cond." and id_parent = ".$bedieningid;	
			$result=$db->query($statement);
		}
		return $result;					
		
	}

}