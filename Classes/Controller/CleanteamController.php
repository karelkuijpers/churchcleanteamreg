<?php

namespace Parousia\Churchcleanteamreg\Controller;

/***************************************************************
*  Copyright notice
*
*  (c) 2019 T.Duinker <tobby.duinker@gmail.com>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Configuration\ConfigurationManager;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Extbase\Http\ForwardResponse;

/**
 * CleanteamController
 *
 */
class CleanteamController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

/**
     * @var \Parousia\Churchcleanteamreg\Domain\Repository\CleanteamRepository
     */
    protected $CleanteamRepository;
	protected $bedieningsid;
	protected $ThisMonth='';
	protected $ThisYear="";
	protected $startnewseason='';
	protected $now='';
	protected $Errmsg;
	protected $db ;


    /**
     * Inject a cleanteam repository to enable DI
     *
     * @param \Parousia\Churchceanteamsreg\Domain\Repository\CleanteamRepository $cleanteamRepository
     */
    public function injectCleanteamRepository(\Parousia\Churchcleanteamreg\Domain\Repository\CleanteamRepository $cleanteamRepository)
    {
        $this->CleanteamRepository = $cleanteamRepository;
    }

  /**
   * action cleanteamview
   * 
   * @return void
   */

  public Function cleanteamviewAction(): ResponseInterface {
		// determine current season:
		$Errmsg='';
		$case=$this->InitSituation();
		if (empty($Errmsg))
		{
			$newseason= $this->ThisYear."-09 - ".($this->ThisYear+1)."-08";
			//$prvseason=($this->ThisYear - 1)."-09 - ".$this->ThisYear."-08";
			$question='Het is nog niet de tijd om cleanteams samen te stellen';
			$weekgeenschoonmaak= GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('churchcleanteamreg','nocleanteamweeks');

			if ($case==5 or $case==2 or $case==1) $question="Cleanteams samenstellen voor seizoen ".$newseason;
			elseif ($case==3) $question="Cleanteams opnieuw samenstellen voor seizoen ".$newseason;
			if ($case==4)
			{
				// check if old year present:
				$row=$this->CleanteamRepository->findBediening("cleanteam", $this->bedieningsid,'asc');	
				$oldyear=substr(trim(explode('&',$row['omschrijving'])[0]),-7);			
			}
			if ($case<6 and $case<>4) $thisaction='cleanteamgenerate';
			if ($case==4 and $this->startnewseason->format("Y-m")>$oldyear) $delete=1; else $delete=0;
		}
        $assignedValues = [
			'errmsg' => $Errmsg,
			'question' => $question,
			'case' => $case,
			'nextyear'=> $this->ThisYear+1,
			'delete' => $delete,
			'nocleanweeknbrs' => $weekgeenschoonmaak,
        ];
        $this->view->assignMultiple($assignedValues);                
		return $this->htmlResponse();	
	}

  /**
   * action cleanteamgenerate
   * 
   * @return void
   */

  public Function cleanteamgenerateAction(): ResponseInterface {
		$Errmsg='';
		$args=$this->request->getArguments();
		$weekgeenschoonmaak=$args['weekgeenschoonmaak'];
		$LocalConfiguration = GeneralUtility::makeInstance(ConfigurationManager::class);
		$LocalConfiguration->setLocalConfigurationValueByPath('EXTENSIONS/churchcleanteamreg/nocleanteamweeks', $weekgeenschoonmaak);
		$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['churchcleanteamreg']['nocleanteamweeks']=$weekgeenschoonmaak;
  		$case=$this->InitSituation();
  		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."cleanteamgenerateAction Weekgeenschoonmaak: ".$weekgeenschoonmaak.'; case: '.$case."; parent:".$this->bedieningsid ."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
	
		if (empty($Errmsg))	$message=$this->ComposeCleanteams($case,$weekgeenschoonmaak);
        $assignedValues = [
			'errmsg' => $Errmsg,
			'question' => $message,
        ];
        $this->view->assignMultiple($assignedValues);           
  		return $this->htmlResponse();	
  }
  
  /**
   * action cleanteamdelete
   * 
   * @return void
   */

  public Function cleanteamdeleteAction(): ResponseInterface {
		$Errmsg='';
  		$case=$this->InitSituation();
  		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."cleanteamgenerateAction Weekgeenschoonmaak: ".$weekgeenschoonmaak.'; case: '.$case."; parent:".$this->bedieningsid ."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
		if (empty($Errmsg))	
		{
			$message=$this->RemoveCleanteams('old');
			if (empty($message)) $message="Alle gewenste teams verwijderd"; 
		}
        $assignedValues = [
			'errmsg' => $Errmsg,
			'question' => $message,
        ];
        $this->view->assignMultiple($assignedValues);                
		return $this->htmlResponse();
  }


  	/**
	 * The ComposeCleanteams method of the PlugIn
	 *
	 * @param	case	
	 */
	function ComposeCleanteams($case,$weekgeenschoonmaak){
    	$datestart=$this->startnewseason->format("Y-m-d");
		$wkfirst=$this->startnewseason->format("W");
		$wkgeenschoonmaak=array();
		if (!empty($weekgeenschoonmaak))$wkgeenschoonmaak=preg_split("/[\s,]+/",$weekgeenschoonmaak);

		// case=1; new season not yet generated and old still valid
		// case=2; current season passed and no new generated
		// case=3; new allready generated and old stil valid
		// case=4; new allready generated and allready valid in autumn
		// case=5; nothing generated up till now
		// case=9; niets doen

		// verwijder alle cleanteams en medewerkers:
		
		// Bewaar eerst alle huidige coordinatoren:                                                    
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'ComposeCleanteams bedieningid:'.$this->bedieningsid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');

		$coordinators=$this->CleanteamRepository->findCoordinators($this->startnewseason,$case, $this->bedieningsid);	
		$coords=explode(',',$coordinators);

		if ($case==3 )
		{
			// verwijder alle cleanteams medewerkers van lopend seizoen om opnieuw samen te stellen:
			 $this->RemoveCleanteams('renew'); 
		}
		// find households per zipcode:
		$adresses=$this->CleanteamRepository->findAdresses($this->now);
		$adressesldr= array();
		$aanttotal=1;
		$aantadresses=count($adresses);
		$aantalteams=floor((52-count($wkgeenschoonmaak)) / 2);
		$teamsize=$aantadresses/$aantalteams;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."Cleanteams aantal teams: ".$aantalteams."; teamsize:".$teamsize."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
		
		$teamsize_int=intval($teamsize);
		$fieldarray_bediening=array('id_parent'=>$this->bedieningsid,'soort'=>'bediening','taakvisie'=>'Het schoonhouden van ons gebouw zodat we er samen in kunnen leven');
		$fieldarray_taak=array('duur'=>'1','tijdsbeslag'=>'2 uur/halfjaar','lidmaatschap'=>'geen lid','aantal'=>'20');
		$pcprvs='';
		$aWknrs=range(1,52);		

		// remove skipped weeks:
		foreach ($wkgeenschoonmaak as $wkgeen)
		{
			if (($key = array_search($wkgeen, $aWknrs)) !== false) {
 			   unset($aWknrs[$key]);
			}
		}
		// reindex aWwknbrs
		$aWknrs= array_values($aWknrs);
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."Aantal teams:".$aantalteams."; Cleanteams weken na schoning: ".http_build_query($aWknrs,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
		// bepaal weekparen:
		$aWkteam=array();
		for ($tm=0;$tm<$aantalteams;$tm++)
		{
			$aWkteam[]=array($aWknrs[$tm],$aWknrs[$tm+$aantalteams]);
		}
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."Cleanteams weeknrs paren: ".http_build_query($aWkteam,'',', ')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
		shuffle($aWkteam);
	
		// Determine team names:
		$ateams=array();
		for ($tm=0;$tm<$aantalteams;$tm++)
		{
			$wk1=$aWkteam[$tm][0];
			$wk2=$aWkteam[$tm][1];
		//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."Cleanteams weektoewijzing aWknrs[$tm]=".$wk1."; aWknrs[$tm+$aantalteams]=".$wk2.";  "."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');

			if (($wk2)>=$wkfirst)$wkpad2=$this->ThisYear; else $wkpad2=$this->ThisYear+1;
			$wkpad2.='-'.str_pad((string)$wk2, 2, '0', STR_PAD_LEFT);
			if ($wk1>=$wkfirst)$wkpad1=$this->ThisYear; else $wkpad1=$this->ThisYear+1;
			$wkpad1.='-'.str_pad((string)$wk1, 2, '0', STR_PAD_LEFT);
			array_push($ateams,'cleanteam wk '.(($wkpad1<=$wkpad2)?$wkpad1.' & '.$wkpad2:$wkpad2.' & '.$wkpad1));
		}
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."Cleanteamnamen : ".implode(" ; ",$ateams)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
		$noleaders=array();
		$updatebasic_bediening="id_parent=".$this->bedieningsid.",soort='bediening',taakvisie='Het schoonhouden van ons gebouw zodat we er samen in kunnen leven'";
		$updatebasic__taak="duur=1,tijdsbeslag='2 uur/halfjaar',lidmaatschap='geen lid',aantal='20'";

		for ($ct=1; $ct<=$aantalteams; $ct++){
			$aant=0;
			$ptr=0;
			$ldr=false;
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."ComposeCleanteams wk: $wk, teamsize_int: $teamsize_int, datestart:$datestart"."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
			while (($aant<$teamsize_int or $aanttotal<(intval($ct*$teamsize)+5) or $ct==$aantalteams) && $aanttotal<=$aantadresses)
			{
				$address=$adresses[$aanttotal-1];
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."ComposeCleanteams adres($ct):".TYPO3\CMS\Core\Utility\GeneralUtility::array2xml($address)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
				if ($aant==0)
				{
					// insert bediening
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."ComposeCleanteams team ;".$ateams[$ct-1]."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
					$ldr=false;
					$ids=$this->CleanteamRepository->insertBedieningTaak($this->db, $ateams[$ct-1],$updatebasic_bediening,$updatebasic__taak);
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."ComposeCleanteams ids ;".urldecode(http_build_query($ids,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
				}
				//check if this address fits most to current week or next
				$pccur=(int)substr($address['postcode'],0,4);
				if (isset($adresses[$aanttotal]))$pcnext=(int)substr($adresses[$aanttotal]['postcode'],0,4);else $pcnext=0;
				// check if address contains leader:
				$persns=explode(',',$address['persids']);
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."ComposeCleanteams wk: $wk aant:$aant, aanttotal:$aanttotal; pcprvs:$pcprvs; pccur:$pccur; pcnext:$pcnext; pcnext-pccur=".($pcnext-$pccur)."; pccur-pcprvs=".($pccur-$pcprvs)."; gr:".";leider:"."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."ComposeCleanteams wk: $wk condition:".(($aant+1)<$teamsize_int or ($pcnext-$pccur)>($pccur-$pcprvs) or $ct==25 or (!$leaderassigned and $leader))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
				if (($aant+1)<($teamsize_int-2) or ($pcnext-$pccur)>=($pccur-$pcprvs) or $ct==$aantalteams )
				{
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."ComposeCleanteams wk: $wk toevoegen aant:$aant, aanttotal:$aanttotal; adresid:".$address['id']."; postcd:".$address['postcode']."; persons:".$address['persids'].";"."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
					$pcprvs=(int)substr($address['postcode'],0,4);
					foreach ($persns as $pers)
					{
						// insert taakbekleding:
						//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."ComposeCleanteams insert medewerkers taak_id;".$ids['taak_id'].'; persid: '.$pers.'; date start: '.$datestart."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
						$result=$this->CleanteamRepository->insertTaakbekleding($this->db, $ids['taak_id'], $pers,$datestart);
						if (!$result) return 'medewerker '. $pers.' onder '.$ateams[$ct-1].' kon niet toegevoegd worden('.$this->db->error.')';
						if (is_array($coords) and in_array ( $pers , $coords))
						{
							// add person as bedieningsleider:
							$ldr=true;
							//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."ComposeCleanteams pers ldr:".$pers." toevoegen aan $wk ".";"."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
							//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."ComposeCleanteams update coordinators statement: ".$GLOBALS['TYPO3_DB']->debug_lastBuiltQuery."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
							$result=$this->CleanteamRepository->updateBedieningsleider($this->db, $ids['teambediening_id'], $pers);	
							if (!$result) return 'bedieningsleider van '.$ateams[$ct-1].' kon niet ingevuld worden';
						}

					}
					$aant++;
					$aanttotal++;
				}
				else 
				{
					break;
				}
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."ComposeCleanteams volgende adres: aant:$aant; teamsize_int:$teamsize_int; aanttotal:$aanttotal; ct:$ct; maxcum: ".intval($ct*$teamsize)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
			}
			if (!$ldr)array_push($noleaders,$ateams[$ct-1]);			
		}
		if ($case<=4) $this->RemoveCleanteams('old');  // remove old year not longer valid
		$returnmsg="$aantadresses adressen verdeeld over de $aantalteams clean teams met gemiddelde grootte ".$teamsize." adressen";
		if (!empty($noleaders))$returnmsg.="<br>Geen leiders gevonden voor:<br>&nbsp;&nbsp;&nbsp;&nbsp;".implode("<br>&nbsp;&nbsp;&nbsp;&nbsp;",$noleaders);
		return $returnmsg;
	}

	
	/**
	 * The InitSituation method of the PlugIn
	 *
	 */
	function InitSituation(){
		// determine current season:
		// Zoek bediening "Schoonmaken gebouw"
		$row=$this->CleanteamRepository->findBediening("Schoonmaken gebouw");
		if (empty($row))
		{
			$this->Errmsg='Maak eerst bediening "Schoonmaken gebouw" aan';
			return 9;
		}
		else
		{
	        $this->bedieningsid = $row['uid'];
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'InitSituation bedieningsid:'.$this->bedieningsid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
		}
	//	$this->now=new \DateTime("2020-09-21");
		$this->now=new \DateTime();
		$this->ThisYear=$this->now->format("Y");
		$this->ThisMonth=$this->now->format("n");
		$this->startnewseason=new \DateTime();
		$this->startnewseason->setISODate($this->ThisYear, 37 ,1);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'InitSituation startnewseason:'.$this->startnewseason->format('d-m-Y')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
		
		$row=$this->CleanteamRepository->findBediening("cleanteam", $this->bedieningsid);	
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'InitSituation cleanteams result:'.urldecode(http_build_query($row,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
		$case=9; // niets doen
		if (!$row) $case=5; // nothing generated up till now
		else
		{
			$lastyear=substr(trim(explode('&',$row['omschrijving'])[1]),0,4);
			if ($lastyear==$this->ThisYear) 
			{
				if ($this->ThisMonth<9)
				{	if ($this->ThisMonth>3) $case=1;} // new season not yet generated and old still valid
				else $case=2; // current season passed and no new generated
			}   
			elseif ($lastyear==($this->ThisYear + 1))
			{
				if ($this->ThisMonth<9) $case=3; // new allready generated and old stil valid
				else $case=4; // new allready generated and allready valid in autumn
			}
			else $case=2; // current season passed and no new generated
		}
		return $case;
	}
	
	/**
	 * The RemoveCleanteams method of the PlugIn
	 *
	 * @param	none	
	 */
	function RemoveCleanteams($type){
		// remove cleanteams 
		//		$type= "renew"
		//				"old"
		
		// determine year and weeknbr max:
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."remove Cleanteams type: ".$type."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
		if ($type=="old")
		{
			$wknow=$this->now->format('Y-').str_pad($this->now->format('W'), 2, '0', STR_PAD_LEFT);
			$wknew=$this->startnewseason->format('Y-').str_pad($this->startnewseason->format('W'), 2, '0', STR_PAD_LEFT);
			$wkmin=min($wknow,$wknew);
			$cond="substr(omschrijving,24,7) <'$wkmin'";
		}
		elseif ($type=="renew")
		{
			$wknew=$this->startnewseason->format('Y-').str_pad($this->startnewseason->format('W'), 2, '0', STR_PAD_LEFT);
			$cond="omschrijving >'cleanteam wk ".$wknew." & '";		
		}
		else return '';
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": "."remove Cleanteams type: ".$type."; cond:".$cond."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchcleanteamreg/Classes/Controller/debug.log');
	
		$result=$this->CleanteamRepository->removeCleanteam($this->db, $this->bedieningsid, $cond);
		if (!$result){
		 	$sqlerr=$this->db->error;
			if (!empty($sqlerr)) return "Kan huidige cleamteams niet verwijderen: $sqlerr";
		} 
		return '';		
	}

}

