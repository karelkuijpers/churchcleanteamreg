<?php
defined('TYPO3') || die('Access denied.');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
         'Churchcleanteamreg',
         'Cleanteam',
         [
             \Parousia\Churchcleanteamreg\Controller\CleanteamController::Class => 'cleanteamview, cleanteamgenerate, cleanteamdelete,',
         ],
         // non-cacheable actions
		 [
             \Parousia\Churchcleanteamreg\Controller\CleanteamController::Class => 'cleanteamview, cleanteamgenerate, cleanteamdelete,',
		],
     );
