function LTrim(str)
{  var whitespace = new String(" \t\n\r");
   var s = new String(str);
   if (whitespace.indexOf(s.charAt(0)) != -1) {
      var j=0, i = s.length;
      while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
         j++;
      s = s.substring(j, i);
   }
   return s;
}
function RTrim(str)
{  var whitespace = new String(" \t\n\r");
   var s = new String(str);
   if (whitespace.indexOf(s.charAt(s.length-1)) != -1) {
      var i = s.length - 1;       // Get length of string
      while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
         i--;
      s = s.substring(0, i+1);
   }
   return s;
}
function Trim(str)
{  return RTrim(LTrim(str));}
function ConvDateFromLocal(fieldname)
{	ds=document.form1[fieldname].value;
	dy=Number(ds.substr(6,4));dm=Number(ds.substr(3,2))-1;dd=Number(ds.substr(0,2));
    CurDate = new Date(dy,dm,dd); 
    return CurDate.toString();
}
function IsDatum(str)
{	if (str.length>0 && str!="00-00-0000")
	{	
		if (/^(0?[1-9]|[1-2]\d|3[0-1])\-(0?[1-9]|1[0-2])\-(19|20)\d{2}$/.test(str))
		{
			var sd = str.split("-");
			var year=sd[2];
			if (sd[1]==2 && sd[0]>(((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 ))return false;
		}
		else return false;
	}
	return true;
}
function daysInFebruary (year){

      // February has 29 days in any year evenly divisible by four,

    // EXCEPT for centurial years which are not also divisible by 400.

    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );

}


function FormatDate(datum)
{
	// maak datum formaat dd-mm-jjjj
	var str=(Trim(datum.value)).toLowerCase();
	if (datum.value.length==0)return true;
	var re= /[/\s\.-]+/g
	str=str.replace(re,"-");
	re=/-+/g
	str=str.replace(re,"-");
	re=/([a-z]+)(\d+)/g
	str=str.replace(re,"$1-$2"); // splits tekst en getallen
	re=/(\d+)([a-z]+)/g
	str=str.replace(re,"$1-$2"); // splits tekst en getallen
	var sd = str.split("-");
	if (sd.length==1)
	{	if (!isNaN(sd[0]) && str.length>4)	str=str.substr(0,2)+"-"+str.substr(2,2)+"-"+str.substr(4);else str="01-01-"+sd[0];
		sd=str.split("-");
	}
	if (sd.length==2){str="01-"+sd[0]+"-"+sd[1];sd=str.split("-");}
	// vertaal eventuele maandnaam:
	if (isNaN(sd[1]))
	{/*	var mm={"jan":"1","feb":"2","maa":"3","apr":"4","mei":"5","jun":"6","jul":"7","aug":"8","sep":"9","okt":"10","oct":"10","nov":"11","dec":"12"};
		var mn=mm[sd[1].substr(0,3)]; */
		var mm={"ja":"01","f":"02","ma":"03","ap":"04","me":"05","jun":"06","jul":"07","au":"08","s":"09","o":"10","n":"11","d":"12"};
		var mn=mm[sd[1].substr(0,3)];
		if (!mn) mn=mm[sd[1].substr(0,2)];
		if (!mn) mn=mm[sd[1].substr(0,1)];
		if (mn)	sd[1]=mn; else sd[1]="01";
	}
	if (isNaN(sd[0])) sd[0]="01";
	if (isNaN(sd[1])) sd[1]="01";
	if (!isNaN(sd[2])&&sd[2].length<3)
	{	if (sd[2].length<2) sd[2]="0"+sd[2];
		if (sd[2].length<2) sd[2]="0"+sd[2];
		d=new Date();
		yr=(d.getFullYear()%100)+1;
		if (sd[2]>yr)sd[2]="19"+sd[2];else sd[2]="20"+sd[2];
	}
	if (sd[1].length<2)	{sd[1]='0'+sd[1];}
	if (sd[0].length<2)	{sd[0]='0'+sd[0];}
	str=sd[0]+"-"+sd[1]+"-"+sd[2];
	datum.value=str;
	if (!IsDatum(str))
	{	alert("Ongeldige datum!");
		return false;
	}
	return true;
}
function validateEmail(str)
{
	var re;
        // Rules for the email regular expression:
        // The start of the email must have at least one character 
        // before the @ sign
        // There may be either a . or a -, but not together before the @ sign
        // There must be an @ sign
        // At least once character must follow the @ sign
        // There may be either a . or a -, but not together in the address
        // The address must end with a . followed by at least 2 characters
	re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/;
	if (str.length>0) {return re.test(str);}
	return true;
}
function ascan(arr,searchvalue,start)
// case insensitive zoeken van searchvalue binnen array arr vanaf start(default 0); 
// geeft terug gevonden positie; -1 indien niet gevonden
{	var i = 0;
	var istart=0;
	var zoek = searchvalue.toLowerCase();
	if (arguments.length>2) {istart=start}
	for (i=istart; i<arr.length;i++)
	{	if (zoek==arr[i].toLowerCase()){return i;}}
	return -1;
}
function HeeftPermissie(permissie){
var pattern = /\s*,\s*/
var permissiegevr=permissie.split(pattern);
for (i=0;i<permissiegevr.length;i++)
{	if (ascan(permissies,permissiegevr[i])>=0){return true;}}
if (ascan(permissies,"admin")>=0){return true;}
return false;
}

function KopTitel(titel)
{	// write header to parent frame
	objelem1=parent.document.getElementById("hoofdtitel");  //TD
	if (objelem1){
		oldDiv=parent.document.getElementById("titeltext"); //span
		newDiv=parent.document.createElement("span");
		newDiv.setAttribute("id","titeltext")
		newText = parent.document.createTextNode(titel);
		newDiv.appendChild(newText);
		objelem1.replaceChild(newDiv,oldDiv);
	}
}



function loadIframe(iframeName, url) {
	//alert("iframeName:"+iframeName+"; URL:"+url) 
  if ( window.frames[iframeName] ) {
    window.frames[iframeName].location = url;   
    }
}

function openIframe(iFrameId,winURL)
{
	ifId=gmobj(iFrameId)
	ifId.src=winURL
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

// Create a cookie with the specified name and value.
// The cookie expires after 100 year.
function SetCookie(sName, sValue)
{
  date1 = new Date();
  date1.setYear(date1.getYear()+100);
  document.cookie = sName + "=" + escape(sValue) +"; path=/; expires=" + date1.toUTCString();
}
function GetCookie(sName)
{ // cookies are separated by semicolons
  var aCookie = document.cookie.split("; ");
  for (var i=0; i < aCookie.length; i++)
  { // a name/value pair (a crumb) is separated by an equal sign
    var aCrumb = aCookie[i].split("=");
    if (sName == aCrumb[0]) return unescape(aCrumb[1]);
  }
  // a cookie with the requested name does not exist
  return "";
}
//--------------------------------------------------------------------------
// scroll to first selected of multiple selection 
//--------------------------------------------------------------------------

function scroll_selected ( form ) 
{
  var form = ( typeof form == 'string' ) ?  document.getElementById(form) : form;
  var selects = form.getElementsByTagName('select');
  OUTER: for ( var i = 0; i < selects.length; i ++ ) 
  {
  	var opts = selects.item(i).getElementsByTagName('option');
  	for ( var j = opts.length -1; j > 0; --j ) 
	{
  		if ( opts.item(j).selected == true ) 
		{
		  selects.item(i).scrollTop = j * opts.item(j).offsetHeight;
		  opts.item(j).selected = true;
		  continue OUTER;
		 }
 	}
  }
}

//--------------------------------------------------------------------------
// multiple selection without CTRL
//--------------------------------------------------------------------------
var arrOldValues;

function FillListValues(CONTROL)
{	// set options previous+new:
	var intNewPos = CONTROL.selectedIndex
	if (intNewPos>=0)// restore previous selected options:
	{	for(var i=0;i<arrOldValues.length;i++){if(arrOldValues[i]){CONTROL.options[i].selected= true;}}
		// toggle selection:	
		if(arrOldValues[intNewPos]){CONTROL.options[intNewPos].selected =false;}else {CONTROL.options[intNewPos].selected =true;}
	}
}

function GetCurrentListValues(CONTROL){
// get previous selections
arrOldValues = new  Array(CONTROL.length);
for(var i = 0;i < CONTROL.options.length;i++){arrOldValues[i]=CONTROL.options[i].selected}
//for(var i = 0;i < CONTROL.length;i++){arrOldValues[i]=CONTROL.options[i].selected}
//CONTROL.selectedIndex=-1 //deselect all
}


function multiSelect( elemId )
{  // select multiple without CTRL key; cal with javascript: multiSelect( "id of select" ) to activate;

 var table = [], box = document.getElementById(elemId), lastSelected = -1, tempIdx, ctrl = false;  
// initialze table with preselected items:
for(var i = 0;i < box.options.length;i++){ if (box.options[i].selected)table.push(i); } 
   
 box.onkeyup = box.onkeydown = readCtrlKey; 
 
 box.onclick = function( /*28432953637269707465726C61746976652E636F6D*/ )
 {
  if( !ctrl )
  {  
   if( !inTable( this.selectedIndex ) )
    table.push( this.selectedIndex );  
   else
    removeFromTable( this.selectedIndex ); 
 
   tempIdx = this.selectedIndex;  
  
   for( var i=0, len = this.options.length; i<len ; i++ ) 
    if( i != this.selectedIndex )
     this.options[ i ].selected = inTable( i );   
    
   this.options[ this.selectedIndex ].selected = inTable( this.selectedIndex );     
  
   lastSelected = tempIdx;
  }
 }
 
 function removeFromTable( idx )
 {
  for( var i=0, len=table.length; i<len && table[i]!=idx; i++ )
  ;      
  if(i != len)
   table.splice(i, 1);
 }
 
 function inTable( idx )
 {
  for( var i=0, len=table.length; i<len && table[i]!=idx; i++ )
  ;  
  return i != len;  
 }
 
 function readCtrlKey( evt )
 {
  var e = evt || window.event;  

  if( typeof( e.ctrlKey ) != 'undefined')
   ctrl = e.ctrlKey;   
 } 
}


var calname;
function OpenCalflight(FORMOBJ,DAYOBJ,MONTHOBJ,KIND) {
DAY   = DAYOBJ.value;
MONTH = MONTHOBJ.value;
URL = pad+"/inc/calender.php?&FORMOBJ=" + FORMOBJ + "&DAYOBJ=" + DAYOBJ + "&MONTHOBJ=" + MONTHOBJ + "&MONTH=" + MONTH + "&KIND=" + KIND + "&DAY=" + DAY;
calname = window.open( URL, "Calender", "width=480,height=200,resizable=1,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,alwaysRaised=1");
calname.location = URL;
calname.focus();
}

